const mongoose = require('mongoose');
const idvalidator = require('mongoose-id-validator');

const AlbumSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
  },
  artist: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Artist',
    required: true,
  },
  year: String,
  image: String
});

AlbumSchema.plugin(idvalidator);
const Album = mongoose.model('Album', AlbumSchema);
module.exports = Album;

// Название
// Исполнитель
// Год выпуска
// Изображение обложки